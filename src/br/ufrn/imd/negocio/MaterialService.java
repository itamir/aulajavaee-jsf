package br.ufrn.imd.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ufrn.imd.dao.MaterialDao;
import br.ufrn.imd.dominio.Material;
import br.ufrn.imd.exceptions.NegocioException;

@Stateless
public class MaterialService {

	@Inject
	private MaterialDao materialDao;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Material salvarOuAtualizar(Material material) 
			throws NegocioException {
		//verificar se o material existe
		Material materialBd = materialDao.buscarMaterial
				(material.getCodigo());
		if(materialBd == null || material.getId() > 0) 
			materialDao.salvarOuAtualizar(material);
		else
			throw new 
			NegocioException("Material existente.");
		return material;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Material material) {
		materialDao.remover(material);
	}
	
	public List<Material> listar() {
		return materialDao.listar();
	}
	
}
