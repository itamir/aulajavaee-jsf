package br.ufrn.imd.resources;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.ufrn.imd.dominio.Material;
import br.ufrn.imd.negocio.MaterialService;

@Stateless
@Path("/consulta")
public class MaterialResource {

	@EJB
	private MaterialService service;

	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/materiais")
	public List<Material> listagem() {
		return service.listar();
	}
}
